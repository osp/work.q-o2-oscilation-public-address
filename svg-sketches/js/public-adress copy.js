let number = 4;

let positions = []

positions.push([0.5,0])
for (let i = 0; i < number; i++) {
    positions.push([Math.random(), Math.random()]);
}
positions.push([0.5,1])

data = "";
possible_move_type = ["S","T"];

function mapValue(value, min, max, new_min, new_max){
    return (((value - min) / (max - min)) * (new_max - new_min)) + new_min;
}

function randomPoint(){
    return [Math.random(), Math.random()];
}

function add_point(move_type, end_point, end_control_point){
    let segment = move_type;

    if(end_control_point){
        segment = segment + end_control_point.join(",") + " ";
    }

    segment = segment + end_point.join(",") + " ";
    return segment;
}

let move_type = "S";

// move to initial position
data = data + add_point("M", positions[0].map(x => mapValue(x, 0, 1, 0, 1000).toFixed(0)));

for (let i = 0; i < positions.length; i++){
    
    let end_point, end_control_point;
    let margin = 200;
    // let margin = 0;

    if(Math.random() > 1){
        move_type = "T";
        end_point = positions[i];
        end_control_point = positions[i];
    }
    else{
        move_type = "S";
        end_point = positions[i];
        end_control_point = positions[i+1];  
    }

    if (i==0){
        // move_type = "S";
        end_control_point = [positions[i][0], positions[i][1] - 0.25];
        margin = 0;
    }
    if (i==positions.length-1){
        // move_type = "S";
        end_control_point = [positions[i][0], positions[i][1] - 0.25];
        margin = 0;
    }

    end_control_point = end_control_point.map(x => mapValue(x, 0, 1, margin, 1000-margin).toFixed(0));
    end_point = end_point.map(x => mapValue(x, 0, 1, margin, 1000-margin).toFixed(0));

    data = data + add_point(move_type, end_point, end_control_point);
}

// actually closing path
// data = data + add_point(move_type, positions[0], [positions[0][0], positions[0][1] - 0.1]);
// data = data + "Z";

console.log(data);
$("#sonic-crowd-network").attr("d", data);


function people_connect(){
    let data = $("#sonic-crowd-network").attr("d");

    let move_type, end_point, end_control_point;

    data_split = data.split("T");
    data = data_split.slice(0, -1).join("T");
    data_end = "T" + data_split[data_split.length - 1];

    if(Math.random() > 0){
        move_type = "T";
        end_point = randomPoint();
        end_control_point = end_point;
    }

    let margin = 200;
    end_control_point = end_control_point.map(x => mapValue(x, 0, 1, margin, 1000-margin).toFixed(0));
    end_point = end_point.map(x => mapValue(x, 0, 1, margin, 1000-margin).toFixed(0));

    data = data + add_point(move_type, end_point, end_control_point);
    data = data + data_end;

    $("#sonic-crowd-network").attr("d", data);
    console.log(data);
}