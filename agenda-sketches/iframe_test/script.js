window.onload = function (){

/*-------------------------------------------------------*/
    $("#div1").resizable();
    $('#div1').resize(function() {
      $('#div2').width($("#parent").width() - $("#div1").width());
    });

    $("#s1").resizable();
    $('#s1').resize(function() {
      $('#div5').height($("#parent").height() - $("#s1").height());
    });

    $("#s2").resizable();
    $('#s2').resize(function() {
      $('#s5').height($("#div5").height() - $("#s2").height());
    });

    $("#div3").resizable();
    $('#div3').resize(function() {
      $('#div4').width($("#div2").width() - $('#div3').width());
    });

    $("#s6").resizable();
    $('#s6').resize(function() {
      $('#s7').height($("#div3").height() - $("#s6").height());
    });

    $("#s3").resizable();
    $('#s3').resize(function() {
      $('#s3').width("#div4").width();
      $('#s4').width("#div4").width();
      $('#s4').height($("#parent").height() - $("#s3").height());
    });
/*--------------Bug fixing Iframe x Resizable-------------------------*/

    var down = false;

    $(document).mousedown(function(){
        down = true;
    }).mouseup(function() {
        down = false;  
    });

    $(document).mousemove(function() {
      if(down) {
        $(".overlay").css("display", "block");
    } 
      else {
        $(".overlay").css("display", "none");
    }
    });

/*------------------Checkboxes----------------------*/

function valueChanged()
{
    if($('.coupon_question').is(":checked"))   
        $(".answer").show();
    else
        $(".answer").hide();
}
$(document).ready(function(){
  $('input[type="checkbox"]').click(function(){
    var val = $(this).attr("value");
    $("." + val).toggle();

  if (!(($('input[value=title]').is(':checked')) || 
    ($('input[value=shape]').is(':checked')) ||
    ($('input[value=about]').is(':checked')) ||
    ($('input[value=blog]').is(':checked'))) 
   ){
    $("#div1").css("flex-grow", "0");
  }
  else{
    $("#div1").css("flex-grow", "1");
  }
  
  if (!(($('input[value=agenda]').is(':checked')) || 
    ($('input[value=specials]').is(':checked'))) 
  ){
    $("#div3").css("flex-grow", "0");
  }
  else{
    $("#div3").css("flex-grow", "1");
  }

  if (!(($('input[value=chat]').is(':checked')) || 
        ($('input[value=live]').is(':checked'))) 
  ){
    $("#div4").css("flex-grow", "0");
  }
  else{
    $("#div4").css("flex-grow", "1");
  }

  });
});



}