
# OscillationTuned Circuits 

<!-- ----------Intro ----------------------
 -->
<section markdown class="intro">
<blockquote> <p>“The interaction of tone, as though a tone were a being. The tone itself may contain a complex spectrum or it may be a more reduced, transparent, or singular vibration. But it is always influenced by what is happening around it, in its periphery, and there are always other tonalities altering its condition.”</p>
<p>— Catherine Lamb</p>
</blockquote> 

<p>Tuned Circuits, the 2021 edition of Oscillation Festival, borrows its title from Daphne Oram, the early electronic composer and instrument inventor. In Oram’s work and writing we glimpse the possibility of a parallel between electronic and biological circuits, and a desire to perceive phenomena simultaneously from various sides. More broadly, the festival looks at practices and phenomena of tuning. Tuning is a fundament of music making. To think in terms of tuning is to think in terms of relations; of one thing coming into consonance or dissonance with another, of one thing colouring and affecting another. It is also to think in terms of time, since tuning requires a process of constant calibration: what is now in tune will not stay that way.</p>


<p>Oscillation—Tuned Circuits takes place over 4 days as a live broadcast from MILL, Brussels and additional locations. The festival will mix talks, performances and works for radio. Each day focusses on a sub-thematic: attuning, as a movement of convergence; feedback, as a circular movement which amplifies itself; detuning, as a movement of unlearning and a condition for regeneration. The opening evening we dedicate to Daphne Oram, whose research thematic we take as our own: ​“to follow curiosities without flinching”.</p>

<p>Oscillation Festival is a project by Q‑O2 werkplaats. This edition is hosted by MILL thanks to Needcompany, with the support of Vlaamse Gemeenschap, Vlaamse Gemeenschapscommissie, City of Brussels, Österreichisches Kulturforum, Nederlandse Ambassade, Ictus.</p>

<p>The Live Audio Stream will be accessible via this website, and diffused on several independent radio stations: Radio p‑node, The Listening Arts Channel, Radio Lyl, Radio Campus Bruxelles, Radio Helsinki, Colaboradio and Radio Shirley&Spinoza.</p>
</section>
<!-- end of intro ----------------------
 -->



<!-- Specials ------------------------------
 -->
 <section markdown class="specials">

<p>Alongside this year’s broadcast, you will here find some special materials that didn’t fit the structure of a radio broadcast. Please note: These materials are only available for the duration of the festival, so be sure not to miss out. </p>
<ul>
<li>Festival Reader</li>
<li>Aura Satz – Oramics: Atlantis Anew [video]</li>
<li>People Like Us – The Mirror [video]</li>
<li>Goodiepal & Pals – Bananskolen [video]</li>
<li>Bill Dietz – Each clap is always also for the world</li>

</ul>
</section>

<!-- end of Specials ------------------------------
 -->
<section markdown class="agenda">
<div id="workshops events">
	<h2>Workshops</h2>

<div class="event">
<date>23.04</date>
<h3>Stellan Veloce</h3> – <h3>Tuning Harmonicas</h3>
</div>
	<div class="event">

<date>24.04</date>

<h3>Farida Amadou</h3> – <h3>Free Improvisation</h3>
	</div>
	<div class="event">

<date>25.04</date>

<h3>Myriam van Imschoot & Andreas Halling</h3> – <h3>Chorus/Insecting</h3>
	</div>
	<div class="event">

<date>26.04</date>

<h3>Liew Niyomkarn</h3> – <h3>Gentle Spring Roll</h3>
	</div>
	<div class="event">

<date>27.04</date>

<h3>Aela Royer</h3> – <h3>Vocal Lab</h3>
	</div>
	<div class="event">

<date>27.04</date>

<h3>Olivia Jack</h3>
 – <h3>Live Coding Visuals</h3>
	</div>
	<div class="event">

<date>28.04</date>

<h3>Sarah van Lamsweerde, Esther Mugambi & Raoul Carrer</h3> – <h3>Agripuncture / Sore Spot Singing</h3>
</div>
	<div class="event">

<date>28.04</date>

<h3>Celeste Betancur</h3> – <h3>Live Coding Sound</h3>
</div>
</div>
<div id="opening events">
	<div class="event">

<date>20:00</date>

<h3>Anna Steiden [talk]</h3>
</div>
	<div class="event">
<date>20:30</date>

<h3>Aura Satz</h3> – <h3>Oramics: Atlantis Anew</h3>
</div>
	<div class="event">
<date>20:40</date>

<h3>Works by Daphne Oram</h3>
</div>
	<div class="event">
<date>21:30</date>

<h3>Jessica Ekomane</h3>
</div>
	<div class="event">
<date>22:00</date>

<h3>People Like Us</h3>
</div>
</div>
</section>
